package constants;

public class Constants {
    public static final String BASE_URL = "https://waarkoop-server.herokuapp.com";

    public static final String  MESSAGE = "detail.message";
    public static final String  ERROR = "detail.error";
    public static final String  REQUESTED_ITEM = "detail.requested_item";
    public static final String  SERVED_BY = "detail.served_by";
}
