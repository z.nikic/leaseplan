package model;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ErrorMessageModel {
    @JsonProperty("detail")
    private Detail detail;

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public static class Detail {
        @JsonProperty("error")
        private boolean error = true;

        @JsonProperty("message")
        private String message = "Not found";;

        @JsonProperty("requested_item")
        private String requestedItem;

        @JsonProperty("served_by")
        private String servedBy = "https://waarkoop.com";

        public boolean getError() {
            return this.error;
        }

        public void setError(Boolean error) { this.error = error; }

        public String getMessage() {
            return this.message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getRequestedItem() {
            return requestedItem;
        }

        public void setRequestedItem(String requestedItem) {
            this.requestedItem = requestedItem;
        }

        public String getServedBy() {
            return this.servedBy;
        }

        public void setServedBy(String servedBy) {
            this.servedBy = servedBy;
        }

    }
}