package model;


import com.fasterxml.jackson.annotation.JsonProperty;


public class SearchModel {
    @JsonProperty("provider")
    private String provider;

    @JsonProperty("title")
    private String title;

    @JsonProperty("url")
    private String url;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("price")
    private String price;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("isPromo")
    private Boolean isPromo;

    @JsonProperty("promoDetails")
    private String promoDetails;

    @JsonProperty("image")
    private String image;

    public String getProvider() {
        return this.provider;
    }
    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getBrand() {
        return this.brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPrice() {
        return this.price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return this.unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPromoDetails() {
        return this.promoDetails;
    }
    public void setPromoDetails(String promoDetails) {
        this.promoDetails = promoDetails;
    }

    public String getImage() {
        return this.image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public boolean getIsPromo() {
        return isPromo;
    }
    public void setIsPromo( boolean isPromo ) {
        this.isPromo = isPromo;
    }
}