package starter.stepdefinitions;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en.And;
import io.restassured.response.ValidatableResponse;
import model.SearchModel;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import org.junit.Assert;
import starter.api.SearchApi;
import model.ErrorMessageModel;

import java.lang.reflect.Type;

import static constants.Constants.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;


public class SearchStepDefinitions {

    @Steps
    public SearchApi searchApi;

    ValidatableResponse response;
    String product;

    @When("I send request for product {string}")
    public void sendRequestForSearchProduct(String productName) {
        product = productName;
        response = searchApi.getSearchProduct(product);
    }

    @Then("I should receive status {string}")
    public void checkStatusCode(String statusCode){
        SerenityRest.restAssuredThat(response -> response.statusCode(Matchers.is(Integer.parseInt(statusCode))));
    }

    @And("the list of product items should be displayed without null values")
    public void checkProductItemsList() throws JsonProcessingException {
        SerenityRest.restAssuredThat(response -> response.body("size()", greaterThan(0)));
        assertElementsDontHaveNullValues(response);
    }

    @And("the response body should be empty")
    public void checkEmptyProductItemsList(){
        SerenityRest.restAssuredThat(response -> response.body("size()", is(0)));
        SerenityRest.restAssuredThat(response -> response.body("isEmpty()", is(true)));
    }

    @And("error message should be displayed")
    public void checkErrorMessage(){
        ErrorMessageModel.Detail expectedErrorMessage = new ErrorMessageModel.Detail();
        expectedErrorMessage.setRequestedItem(product);
        SerenityRest.restAssuredThat(response -> response.body(MESSAGE, is(expectedErrorMessage.getMessage())));
        SerenityRest.restAssuredThat(response -> response.body(ERROR, is(expectedErrorMessage.getError())));
        SerenityRest.restAssuredThat(response -> response.body(REQUESTED_ITEM, is(expectedErrorMessage.getRequestedItem())));
        SerenityRest.restAssuredThat(response -> response.body(SERVED_BY, is(expectedErrorMessage.getServedBy())));
    }

    private void assertElementsDontHaveNullValues(ValidatableResponse response){
        SearchModel[] searchModelArray  = response.extract().as((Type) SearchModel[].class);
        for(SearchModel searchModel : searchModelArray) {
            Assert.assertNotNull("Provider is not null",searchModel.getProvider());
            Assert.assertNotNull("Title is not null",searchModel.getTitle());
            Assert.assertNotNull("Url is not null",searchModel.getUrl());
            Assert.assertNotNull("Brand is not null",searchModel.getBrand());
            Assert.assertNotNull("Price is not null",searchModel.getPrice());
            Assert.assertNotNull("Unit is not null",searchModel.getUnit());
            Assert.assertNotNull("IsPromo is not null",searchModel.getIsPromo());
            Assert.assertNotNull("PromotionDetails is not null",searchModel.getPromoDetails());
            Assert.assertNotNull("Image is not null",searchModel.getImage());
        }
    }
}
