package starter.api;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static constants.Constants.BASE_URL;


public class SearchApi {

    private static final String SEARCH_API_URL = BASE_URL + "/api/v1/search/demo/{product}" ;

    @Step("Get search for product {0}")
    public static ValidatableResponse getSearchProduct(String product){
        return SerenityRest.given().pathParam("product", product).get(SEARCH_API_URL).then();
    }
}
