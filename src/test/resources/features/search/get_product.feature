Feature: Search for the product

  Scenario Outline: Search API returns list of the product items for available products
    When I send request for product "<name>"
    Then I should receive status "<code>"
    And  the list of product items should be displayed without null values
    @Smoke
    Examples:
      | name  | code  |
      | apple | 200   |
    @Regression
    Examples:
      | name  | code  |
      | pasta | 200   |
      | cola  | 200   |

  @Regression
  Scenario Outline: Search API returns empty list if the product does not have items
    When I send request for product "<name>"
    Then I should receive status "<code>"
    And  the response body should be empty
    Examples:
      | name    | code  |
      | orange  | 200   |

  @Regression
  Scenario Outline: Search API returns error message for non-existing products
    When I send request for product "<name>"
    Then I should receive status "<code>"
    And error message should be displayed
    Examples:
      | name    | code  |
      | car     | 404   |
