# Serenity Cucumber project 

## About

Resource under test :
https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product}
        
## Prerequisites
-Java jdk 17.0.2  
-apache-maven-3.8.1 

## Project structure
Step definitions : starter.stepdefinitions.SearchStepDefinitions

API Steps : starter.api.SearchApi

Feature files : resources.features


## Running the tests
In order to execute all tests you need to run maven command : 
```
mvn clean verify serenity:aggregate
```
In order to execute tagged tests you need to run maven command: 
execute only tests that are part of smoke testing: 
```
mvn clean verify -Dcucumber.filter.tags=@<!-- -->SMOKE serenity:aggregate
```
execute only tests that are part of regression testing: 
```
mvn clean verify -Dcucumber.filter.tags=@<!-- -->REGRESSION serenity:aggregate
```

Test results/Serenity reports: ./target/site/serenity/index.html

### Added files in the project
- .gitignore
- .gitlab-ci.yml
- Constants.java into "src/main/java/constants/"
- ErrorMessageModel.java into "src/main/java/model/"
- SearchModal.java into "src/main/java/model/"

### Modified files and folders
- CarApi.java is renamed to SearchApi.java and moved to "src/test/java/starter/api/"
- post_product.feature is renamed to get_product.feature
- SearchStepDefinitions.java 
- TestRunner.java path for feature file is changed from "src/test/resources/features" to "src/test/resources/features/search"
- pom.xml 

  - added in pom.xml
```xml
<properties>
    ...
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <serenity.maven.version>4.0.18</serenity.maven.version>
        <serenity.cucumber.version>2.6.0</serenity.cucumber.version>
        <junit.version>4.13.2</junit.version>
        <mavenfailsafe.version>3.2.1</mavenfailsafe.version>
        <mavencompiler.version>3.8.1</mavencompiler.version>
    ...
</properties>
```
- modified in pom.xml
```xml
    ...
    <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>3.0.0-M5</version>
    ...
        <artifactId>maven-failsafe-plugin</artifactId>
        <version>3.0.0-M5</version>
        <configuration>
          <includes>
            <include>TestRunner.java</include>
          </includes>
    ...
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
            <source>1.8</source>
            <target>1.8</target>
        </configuration>
```
- removed from pom.xml

```xml
    <dependencies>
      ...
        <dependency>
          <groupId>net.serenity-bdd</groupId>
          <artifactId>serenity-screenplay</artifactId>
          <version>${serenity.version}</version>
          <scope>test</scope>
        </dependency>
        <dependency>
          <groupId>net.serenity-bdd</groupId>
          <artifactId>serenity-screenplay-webdriver</artifactId>
          <version>${serenity.version}</version>
          <scope>test</scope>
        </dependency>
        <dependency>
          <groupId>ch.qos.logback</groupId>
          <artifactId>logback-classic</artifactId>
          <version>1.0.13</version>
        </dependency>
        <dependency>
          <groupId>org.assertj</groupId>
          <artifactId>assertj-core</artifactId>
          <version>3.6.2</version>
          <scope>test</scope>
        </dependency>
        <dependency>
          <groupId>org.hamcrest</groupId>
          <artifactId>hamcrest-all</artifactId>
          <version>1.3</version>
        </dependency>
      ...
    </dependencies>
    ...
    <distributionManagement>
        <repository>
            <id>central</id>
            <name>a0jfsyiy82n4q-artifactory-primary-0-releases</name>
            <url>${env.MAVEN_REPO_URL}/default-maven-local</url>
        </repository>
        <snapshotRepository>
            <id>snapshots</id>
            <name>a0jfsyiy82n4q-artifactory-primary-0-snapshots</name>
            <url>${env.MAVEN_REPO_URL}/default-maven-local</url>
        </snapshotRepository>
    </distributionManagement>
    ...
    <configuration>
        ...
        <systemPropertyVariables>
        <webdriver.base.url>${webdriver.base.url}</webdriver.base.url>
        </systemPropertyVariables>
        ...
    </configuration>
```

### Deleted files and folders 
- idea/.
- gradle/.
- target/.
- LICENSE
- build.gradle
- gradlew
- gradlew.bat
- src/test/resources/logback-test.xml

## Model classes
For both existing response payloads are created separate models  
  ErrorMessageModel - for error response
  SearchModel - for correct response 
It's easier to update models if any change on the API is made and we can just remove or add new element into model with their getter/setter methods
It give us oportunity to easier check the correct values if we know them (for example: the correct values in error message) so, any change can be easily tested and detected

## Feature file
It is organized by the tags, so every test can be executed by the tag name or all of them can be executed without tag name
First scenario example values are divided with tags (@Smoke and @Regression) just to use different values depending of the testing that we want to perform, also I don't want to execute the same scenario and the same test to many times (3 times) if the expected behavior should be the same for all 3 products
Also last two scenarios are not part of @Smoke testing, because they are not testing happy paths (which usualy is part of smoke testing), but are neccessary in regression testing
